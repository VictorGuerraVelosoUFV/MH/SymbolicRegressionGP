from SymbolicRegression.GeneticProgramming import main
from deap import base

if __name__ == "__main__":
    toolbox = base.Toolbox()
    pop, log, hof = main(toolbox,1000)
    for i in hof.items:
        print(i.fitness)
        '''
    pop, log, hof = main(500)
    for i in hof.items:
        print(i.fitness)
    pop, log, hof = main(2000)
    for i in hof.items:
        print(i.fitness)
        '''