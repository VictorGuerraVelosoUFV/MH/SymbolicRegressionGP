# -*- coding: UTF-8 -*-

import operator
import math
import random

import numpy

from deap import algorithms
from deap import creator
from deap import tools
from deap import gp
from deap import base

toolbox = base.Toolbox()


def a(Kd, Kp):
    try:
        ret = abs((Kd + 1) ** 2 - 4 * Kp) ** 0.5
    except ValueError:
        return 0
    return ret


def b(Kd, Kp):
    try:
        ret = (1 - Kd) / (a(Kd, Kp))
    except ZeroDivisionError:
        return 0
    return ret


def ise(Kd, Kp):
    try:
        eqs = [-(1 + b(Kd, Kp)) ** 2 / (a(Kd, Kp) - 1 - Kd),
               -2 * (1 - (b(Kd, Kp)) ** 2) / (- 1 - Kd),
               -(1 - b(Kd, Kp)) ** 2 / (-a(Kd, Kp) - 1 - Kd)]
    except ZeroDivisionError:
        return 0
    return sum(eqs)


def evalSymbReg(individual, points, toolbox):
    # Transform the tree expression in a callable function
    func = toolbox.compile(expr=individual)
    # Evaluate the mean squared error between the expression
    # and the real function : x**4 + x**3 + x**2 + x
    sqerrors = (((func(Kd, Kp) - ise(Kd, Kp)) ** 2) for Kd, Kp in points)
    for Kd, Kp in points:
        if type(ise(Kd, Kp)) == type(complex()):
            print(str(Kd) + " " + str(Kp))
    return math.fsum(sqerrors) / len(points),


def secuteDiv(num, den):
    try:
        res = num / den
    except:
        res = 0
    return res


def secureSum(a, b):
    try:
        res = a + b
    except OverflowError:
        res = 0
    return res


def secureMul(a, b):
    try:
        res = a * b
    except OverflowError:
        res = 0
    return res


def secureSub(a, b):
    try:
        res = a - b
    except OverflowError:
        res = 0
    return res


def securePow(base, pow):
    pow = pow % 15
    try:
        res = base ** pow
        if type(res) == complex:
            return 0
    except ZeroDivisionError:
        return 0
    except OverflowError:
        return 0
    return base ** pow


def secureLog(n):
    try:
        res = math.log(n)
    except:
        return 0
    return res


def cfg(toolbox):
    pset = gp.PrimitiveSet("MAIN", 2)
    pset.addPrimitive(secureSum, 2)
    pset.addPrimitive(secureSub, 2)
    pset.addPrimitive(secureMul, 2)
    # pset.addPrimitive(securePow, 2)
    pset.addPrimitive(secureLog, 1)
    pset.addPrimitive(math.sin, 1)
    pset.addPrimitive(secuteDiv, 2)
    J = 3.2284 * (10 ** -6)
    pset.addTerminal(J)
    B = 3.5077 * (10 ** -6)
    pset.addTerminal(B)
    K = 0.0247
    pset.addTerminal(K)
    R = 4
    pset.addTerminal(R)
    L = 2.75 * (10 ** -6)
    pset.addTerminal(L)
    pset.addEphemeralConstant("rand101", lambda: random.uniform(-10, 10))
    pset.renameArguments(ARG0='Kd')
    pset.renameArguments(ARG1='Kp')

    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin)

    toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=1, max_=2)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("compile", gp.compile, pset=pset)

    toolbox.register("evaluate", evalSymbReg,
                     points=[(Kd / 10., Kp / 10.) for Kd in range(-10, 10) for Kp in range(-10, 10)], toolbox=toolbox)
    toolbox.register("select", tools.selTournament, tournsize=2)
    toolbox.register("mate", gp.cxOnePoint)
    toolbox.register("expr_mut", gp.genFull, min_=0, max_=2)
    toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

    toolbox.decorate("mate", gp.staticLimit(key=operator.attrgetter("height"), max_value=7))
    toolbox.decorate("mutate", gp.staticLimit(key=operator.attrgetter("height"), max_value=7))


def main(toolbox, size):
    cfg(toolbox)
    random.seed(318)

    pop = toolbox.population(n=size)
    hof = tools.HallOfFame(1)

    stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
    stats_size = tools.Statistics(len)
    mstats = tools.MultiStatistics(fitness=stats_fit, size=stats_size)
    mstats.register("avg", numpy.mean)
    mstats.register("std", numpy.std)
    mstats.register("min", numpy.min)
    mstats.register("max", numpy.max)

    pop, log = algorithms.eaSimple(pop, toolbox, 0.8, 0.1, 40, stats=mstats,
                                   halloffame=hof, verbose=True)
    # print log
    return pop, log, hof