from setuptools import setup

setup(
    name='SymbolicRegression',
    version='0.1',
    packages=['SymbolicRegression'],
    url='',
    license='BSD',
    author='victorgv',
    author_email='victorgvbh@gmail.com',
    install_requires=[
        "deap",
        "numpy"
    ],
    description=''
)
