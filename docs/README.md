Introdução
==========

Configurações Principais
------------------------

###Conjunto de funções
Como se trata de uma regressão simbólica de uma expressão aritmética, as funções presentes devem ser funções aritméticas.
Como (+,-,*,/,%,!,^,√) e (cos,sin,tg,log,ln,exp)

###Conjunto de terminais
k_p, k_i, k_d and ephemeral constant

####Constantes:
Momento de inércia do rotor (J): 3.2284 * 10^-6 kg.m²
Razão de amortecimento do sistema mecânico (B): 3.5077 * 10^-6 Nms
Força constante eletromotiva (K): 0.0274 Nm/Amp
Resistencia elétrica (R): 4 ohm
Indução elétrica (L): 2.75 * 10^-6 H

###Intervalo da constante efêmera
]-10, 10[ and {1, 2, …, 5}

###Quantidade de programas de computador
500, 1000 or 2000

###Taxas de cruzamento, reprodução e mutação
c.:0.9
r.:0.1
m.:0.2

###Taxa de cruzamento nós internos
0.8

###Método de seleção (torneio - tamanho torneio)
Torneio: 2

###A altura máxima da árvore durante a execução
7

###A altura máxima da árvore para a geração inicial
2

###Método de geração para população inicial
Ramped half-and-half

###Medida de Fitness
MSE com escala de fitness.
![Image](https://imgur.com/download/gB1zOSP "Ou acesse https://ibb.co/doztTT")

###Fitness cases
900

###Critério de parada (ex.: Número máximo de gerações)
Máximo de gerações: 50

Propriedades a serem respeitadas
--------------------------------

###Propriedades de Fechamento
Toda função deve ser capaz de receber como parâmetro o retorno de qualquer outra função implementada.
Para isso deve-se:
- Tratar exceções;
- Utilizar uma tipagem bem definida e flexível.

###Propriedade de Suficiência
Essa propriedade apenas implica que deve-se ter conhecimento da possibilidade de solucionar o problema com o conjunto de
funções e terminais disponíveis.

Detalhes de implementação a serem considerados
----------------------------------------------

###Reprodução
Uma quantidade de programas é selecionada com base na aptidão. A quantia é definida pela taxa de reprodução e tamanho da
população

###Cruzamento
Os pontos de cruzamento devem pertencer ao mesmo conjunto de funções e terminais

###Mutação
####Tipos

- Sub-árvore: Remoção de um ramo e substituição por um outro gerado aleatoriamente
- Sub-árvore tamanho justo: Remoção de um ramo e substituição por um outro de igual tamanho
- Substituição de nó: substitui-se um nó por outro aleatório, se função é por outro de mesma aridade
- Encolhimento: Substituição de nó função por nó terminal aleatório

####Propriedades
- Os pontos de mutação devem pertencer ao mesmo conjunto de funções e terminais
- No caso de nó função, deve-se manter aridade

###Técnicas avançadas

####Operador simplificador
Tratar redundancias como ((x+x)-x)=x

####Elitismo
Manter melhores indivíduos da geração anterior

####Técnicas neutralizadoras do Bloat
- Impor limite de tamanho e profundidade
- Aplicar mutação Sub-árvore tamanho justo

O problema a ser solucionado
============================

![Equação do posicionamento](https://imgur.com/download/ZXkZ5k7 "Ou acesse https://imgur.com/a/c66H14d") do motor de corrente contínua.

Estratégia de desenvolvimento
=============================

Para acelerar o desenvolvimento, será adotada uma postura ágil e uma linguagem de programação simplificada (Python).
A biblioteca [DEAP](http://deap.readthedocs.io/en/master/examples/gp_symbreg.html) será utilizada por ser muito bem
documentada e bem aplicada (como pode ser visto em seu [repositório do Github](https://github.com/DEAP/deap) ).

Contudo para um maior entendimento do problema e facilitação de explicação serão produzidos diagramas de atividade para
representar o fluxo e a lógica por trás do algoritmo implementado.

Literatura e referência
=======================

- [A field guide to genetic programming](http://www0.cs.ucl.ac.uk/staff/ucacbbl/ftp/papers/poli08_fieldguide.pdf) de
Riccardo Poli, Bill Langdon, Nic McPhee

Dificuldades enfrentadas
========================

Soluções encontradas
====================

Como executar
=============